package com.company;

import com.company.app.MyLibraryTopiaApplicationContext;
import com.company.app.MyLibraryTopiaPersistenceContext;
import com.company.app.entities.Author;
import com.company.app.entities.AuthorImpl;
import com.company.app.entities.AuthorTopiaDao;
import com.company.app.entities.Book;
import com.company.app.entities.BookImpl;
import com.company.app.entities.BookTopiaDao;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SampleTest {

    private MyLibraryTopiaApplicationContext applicationContext;

    private MyLibraryTopiaPersistenceContext persistenceContext;

    private AuthorTopiaDao authorDao;

    private BookTopiaDao bookDao;

    @Before
    public void createPersistenceContextAndDaos() {
        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        TopiaConfiguration topiaConfiguration = topiaConfigurationBuilder
                .forH2DatabaseInTempDirectory()
                .onlyCreateSchemaIfDatabaseIsEmpty()
                .doNotValidateSchemaOnStartup()
                .build();
        applicationContext = new MyLibraryTopiaApplicationContext(topiaConfiguration);
        persistenceContext = applicationContext.newPersistenceContext();
        authorDao = persistenceContext.getAuthorDao();
        bookDao = persistenceContext.getBookDao();
    }

    @After
    public void closePersistenceContext() {
        persistenceContext.close();
        applicationContext.close();
    }

    @Test
    public void testCreateEntities() {

        Author platon = new AuthorImpl();
        platon.setName("Platon");
        Assert.assertFalse(platon.isPersisted());

        authorDao.create(platon);
        Assert.assertTrue(platon.isPersisted());

        Book theRepublic = new BookImpl();
        theRepublic.setName("La République");
        theRepublic.setAuthor(platon);
        Assert.assertFalse(theRepublic.isPersisted());
        bookDao.create(theRepublic);

        // another way to create an entity, in one line
        Book gorgias = bookDao.create(
                Book.PROPERTY_AUTHOR, platon,
                Book.PROPERTY_NAME, "Gorgias");

        // from a map
        Map<String, Object> bookProperties = new HashMap<String, Object>();
        bookProperties.put(Book.PROPERTY_AUTHOR, platon);
        bookProperties.put(Book.PROPERTY_NAME, "Le Banquet");
        Book leBanquet = bookDao.create(bookProperties);

        Assert.assertTrue(gorgias.isPersisted());
        Assert.assertTrue(leBanquet.isPersisted());

        persistenceContext.commit();

    }

    @Test
    public void testRetrieveEntities() {

        testCreateEntities();

        Author platon = authorDao
                      . forNameEquals("Platon")
                      . findUnique();

        List<Book> allBooks = bookDao.findAll();
        long booksCount = bookDao.count();
        List<Book> allBooksInAlphabeticalOrder = bookDao.newQueryBuilder().setOrderByArguments(Book.PROPERTY_NAME).findAll();

        Book book = bookDao.forNameEquals("La République").findUnique();
        Book bookOrNull = bookDao.forNameEquals("La République").findUniqueOrNull();
        Optional<Book> bookOptional = bookDao.forNameEquals("La République").tryFindUnique();

        Book firstBook = bookDao.newQueryBuilder().setOrderByArguments(Book.PROPERTY_NAME).findFirst();

        Optional<Book> aBookObtainedViaComplexQuerying = bookDao.newQueryBuilder()
                . addEquals(Book.PROPERTY_AUTHOR, platon)
                . addIn(Book.PROPERTY_NAME,
                        ImmutableSet.of(
                                "La République", "Le banquet"
                        ))
                . setOrderByArguments(Book.PROPERTY_ISBN)
                . tryFindFirst();

        if (aBookObtainedViaComplexQuerying.isPresent()) {
            aBookObtainedViaComplexQuerying.get().getAuthor();
        }

    }

    @Test
    public void testUpdateEntities() {
        testCreateEntities();

        Book book = bookDao.forNameEquals("La République").findUnique();
        book.setIsbn("123456789");
        bookDao.update(book);

        Book anotherBook = bookDao.forNameEquals("Le Banquet").findUnique();
        anotherBook.setIsbn("987654321");
        bookDao.update(anotherBook);

        // commit both changes
        persistenceContext.commit();
    }

    @Test
    public void testDeleteEntities() {
        testCreateEntities();

        Book book = bookDao.forNameEquals("La République").findUnique();
        bookDao.delete(book);

        persistenceContext.commit();

        Assert.assertFalse(bookDao.forNameEquals("La République").exists());

    }
}