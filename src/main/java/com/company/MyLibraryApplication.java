package com.company;

import com.company.app.MyLibraryTopiaApplicationContext;
import com.company.app.MyLibraryTopiaPersistenceContext;
import com.company.app.entities.Author;
import com.company.app.entities.AuthorTopiaDao;
import com.company.app.entities.Book;
import com.company.app.entities.BookTopiaDao;
import com.google.common.base.Optional;
import com.google.common.collect.ImmutableSet;
import org.nuiton.topia.persistence.TopiaConfiguration;
import org.nuiton.topia.persistence.TopiaConfigurationBuilder;

/**
 * This is a minimal sample application that start topia.
 */
public class MyLibraryApplication {

    public static void main(String[] args) {

        TopiaConfigurationBuilder topiaConfigurationBuilder = new TopiaConfigurationBuilder();
        TopiaConfiguration topiaConfiguration = topiaConfigurationBuilder
                                              . forH2DatabaseInTempDirectory()
                                              . onlyCreateSchemaIfDatabaseIsEmpty()
                                              . doNotValidateSchemaOnStartup()
                                              . build();

        MyLibraryTopiaApplicationContext applicationContext =
                new MyLibraryTopiaApplicationContext(topiaConfiguration);

        MyLibraryTopiaPersistenceContext persistenceContext =
                applicationContext.newPersistenceContext();

        AuthorTopiaDao authorDao =
                persistenceContext.getAuthorDao();

        BookTopiaDao bookDao = persistenceContext.getBookDao();

        Author platon = authorDao
                      . forNameEquals("Platon")
                      . findUnique();

        Optional<Book> book = bookDao.newQueryBuilder()
                  . addEquals(Book.PROPERTY_AUTHOR, platon)
                  . addIn(Book.PROPERTY_NAME,
                          ImmutableSet.of(
                                  "La République", "Le banquet"
                          ))
                  . setOrderByArguments(Book.PROPERTY_ISBN)
                  . tryFindFirst();

        if (book.isPresent()) {
            book.get().getAuthor();
        }

        applicationContext.close();

    }

}
