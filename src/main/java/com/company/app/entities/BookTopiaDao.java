package com.company.app.entities;

import org.nuiton.topia.persistence.HqlAndParametersBuilder;

import java.util.HashMap;
import java.util.Map;

public class BookTopiaDao extends AbstractBookTopiaDao<Book> {

    public void aMethodOnBookDaoToRunSql() {
        // topiaSqlSupport.findSingleResult("...");
    }

    public void aMethodToRunPlainHqlAsString() {
        String hql = newFromClause(); // "from Book"
        hql += " where author = select a from Author a where a.name like 'Jules%'";
        Map<String, Object> hqlParameters = new HashMap<String, Object>();
        findAll(hql, hqlParameters);
    }

    public void aMethodToUseHqlBuilder(boolean doSomethingComplex) {
        HqlAndParametersBuilder<Book> builder = newHqlAndParametersBuilder();
        if (doSomethingComplex) {
            builder.addWhereClause("a very complex subquery...");
        }
        builder.addFetch(Book.PROPERTY_AUTHOR);
        String hql = builder.getHql();
        Map<String, Object> hqlParameters = builder.getHqlParameters();
        findAll(hql, hqlParameters);
    }

}
