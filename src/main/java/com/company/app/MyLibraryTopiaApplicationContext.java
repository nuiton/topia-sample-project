package com.company.app;

import org.nuiton.topia.persistence.TopiaConfiguration;

/**
 * A class to add some methods on this project's {@link org.nuiton.topia.persistence.TopiaApplicationContext}.
 */
public class MyLibraryTopiaApplicationContext extends AbstractMyLibraryTopiaApplicationContext {

    public MyLibraryTopiaApplicationContext(TopiaConfiguration topiaConfiguration) {
        super(topiaConfiguration);
    }

    public void doSomethingOnThisApplicationContext() {
        MyLibraryTopiaPersistenceContext myLibraryTopiaPersistenceContext = newPersistenceContext();
        myLibraryTopiaPersistenceContext.doSomethingOnThisPersistenceContext();
        myLibraryTopiaPersistenceContext.close();
    }

}
